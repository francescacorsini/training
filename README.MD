# Course outline

## Stage 1: Getting started

First, go to https://gitlab.com and create an account there if you do not already have one.


#### Go to gitlab
Go to the project we just gave you access to and create a new branch for yourself or clone it.

## Stage 2:
Now that we created our own branch we can start with setting up our first sonarqube scanner.
The information that you need to get the sonar scanner is the following:

```
Url: <sonarqube url>
Api key: <api key / generate a token>
project key: <create this key yourself in sonarqube>
```

The credentials you need to login to sonarqube are:

```
url: http://34.74.11.169:9000
User: admin
Pass: ING-training2018!
```
You need to login into sonarqube to set up your own project.
While setting up your project you get a project key. Use this key
to specify the project to push the metrics to for the sonar-scanner.


#### Set up the scanner
Follow this guide to see how to set up the sonar-scanner docker image in the CI
pipeline:

[Link to sonar scanner how to](https://gitlab.com/RiieCco1/training/tree/master/sonar-scanner)

If you get stuck configuring the pipeline script for sonar-scanner you can refer here to the 
solution:

[Link to sonar-scanner solution](https://gitlab.com/RiieCco1/training/blob/master/sonar-scanner/runner-solution.md)


## Stage 2

#### Grab the API key from defect-dojo
You can login into Defect Dojo on <target> with the following credentials:

```
url: http://35.187.9.130:8080
User: admin
Pass: workshop
```

You need Defect Dojo to congifure your scans, since you need to get the API key and
feed it to the right env variables in the .gitlab-ci.yaml file. Otherwise your scan wil not be able to 
push the metrics to Defect Dojo. 

In defect dojo, add a new product for yourself. After adding the product you will find
the option to add a new CI/CD engagement. Create the new engagement and make a note of
the engagement ID. You will need this ID to tell the scanner where to push the 
metrics to.

After configuring your scan properly you can commit your changes. Gitlab will instantly
start a build and you can follow that through the Gitlab interface.


## Stage 3: Trufflehog

Study the Dockerfile and the entrypoint.sh files to
learn how to run these scans in your CI environment:

Here we see the Dockerfile used to build the image

```docker
FROM python:3.7.3

RUN apt-get update -y && apt-get upgrade -y && apt-get install curl

RUN pip3 install trufflehog

COPY entrypoint.sh entrypoint.sh

RUN chmod +x entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]

```

Here we find the entrypoint.sh content. Study the environment variables that you need to
set in order to initiate a new scan properly.

```bash
#!/bin/bash

set -e

exit_env_error() {
    echo "Error: env var '${1}' not set" >&2
    exit 1
}

[ -z "${SOURCE_REPO}" ] && exit_env_error SOURCE_REPO
[ -z "${DOJO_URL}" ] && exit_env_error DOJO_URL
[ -z "${DOJO_ENGAGEMENT_ID}" ] && exit_env_error DOJO_ENGAGEMENT_ID
[ -z "${DOJO_API_KEY}" ] && exit_env_error DOJO_API_KEY

trufflehog --regex --json --entropy=False "${SOURCE_REPO}" > output.json | exit 0
ls -lart
cat output.json

SCAN_DATE=`date +%Y-%m-%d`

curl --request POST \
    --url "${DOJO_URL}"/api/v1/importscan/ \
    --header 'authorization: ApiKey '"${DOJO_API_KEY}"' ' \
    --header 'cache-control: no-cache' \
    --header 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
    --form minimum_severity=Info \
    --form scan_date="${SCAN_DATE}" \
    --form verified=False \
    --form file=@"${PROJECT_FOLDER}"/output.json \
    --form tags=Test_automation \
    --form active=True \
    --form engagement=/api/v1/engagements/"${DOJO_ENGAGEMENT_ID}"/ \
    --form 'scan_type=Trufflehog Scan'

```

If you get stuck configuring the pipeline you can refer to the solution:
[Trufflehog config solution](https://gitlab.com/RiieCco1/training/blob/master/trufflehog/trufflehog-solution.md)

## Stage 4: Mobsf

Study the Dockerfile and the entrypoint.sh files to learn how to run these scans in your CI environment:
Here we see the Dockerfile used to build the image:

```dockerfile
FROM alpine:latest 

RUN apk update --no-cache && apk add python3 \
curl \
openssl \
bash \
git \ 
jq

WORKDIR scanner

COPY entrypoint.sh /

RUN ["chmod", "+x", "/entrypoint.sh"]
RUN pwd

ENTRYPOINT ["/entrypoint.sh"]
```

And here we see the entrypoint.sh bash script:

```bash
#!/bin/bash

exit_env_error() {
echo "Error: env var '${1}' not set" >&2
exit 1
}

[ -z "${SOURCE_REPO}" ] && exit_env_error SOURCE_REPO
[ -z "${APK_NAME}" ] && exit_env_error APK_NAME
[ -z "${MOBSF_ENGINE_URL}" ] && exit_env_error MOBSF_ENGINE_URL
[ -z "${MOBSF_API_KEY}" ] && exit_env_error MOBSF_API_KEY
[ -z "${DOJO_URL}" ] && exit_env_error DOJO_URL
[ -z "${DOJO_ENGAGEMENT_ID}" ] && exit_env_error DOJO_ENGAGEMENT_ID
[ -z "${DOJO_API_KEY}" ] && exit_env_error DOJO_API_KEY

rm -rf project
git clone "${SOURCE_REPO}" project

curl -F 'file=@/scanner/project/'${APK_NAME}'' ${MOBSF_ENGINE_URL}/api/v1/upload -H "Authorization:${MOBSF_API_KEY}" | jq --raw-output ".hash" > hash.txt
HASH=$(cat hash.txt)

curl -X POST --url ${MOBSF_ENGINE_URL}/api/v1/scan --data "scan_type=apk&file_name=${APK_NAME}&hash=${HASH}" -H "Authorization:${MOBSF_API_KEY}"
echo "//////////////////"
echo $HASH
echo "//////////////////"


curl -X POST --url ${MOBSF_ENGINE_URL}/api/v1/report_json --data "hash=${HASH}&scan_type=apk" -H "Authorization:${MOBSF_API_KEY}" > result.json

SCAN_DATE=`date +%Y-%m-%d`

cat result.json | jq

curl --request POST \
    --url "${DOJO_URL}"/api/v1/importscan/ \
    --header 'authorization: ApiKey '"${DOJO_API_KEY}"' ' \
    --header 'cache-control: no-cache' \
    --header 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
    --form minimum_severity=Info \
    --form scan_date="${SCAN_DATE}" \
    --form verified=False \
    --form file=@result.json \
    --form tags=Test_automation \
    --form active=True \
    --form engagement=/api/v1/engagements/"${DOJO_ENGAGEMENT_ID}"/ \
    --form 'scan_type=MobSF Scan'
```

List of mobsf instances we have:
```
01 - http://35.231.166.133:8000
02 - http://35.196.106.172:8000
03 - http://35.243.216.85:8000
04 - http://35.237.211.82:8000 
05 - http://34.73.223.211:8000 
06 - http://35.237.251.197:8000 
07 - http://34.74.121.55:8000 
08 - http://34.74.25.114:8000
09 - http://35.243.137.213:8000
10 - http://34.74.218.29:8000
```

Now, sadly the plugin for defect dojo is not completely finished yet, but we do find the results in the Mobsf engine 
and output in the CI log files.

If you get stuck find the complete result in the following file:

[Mobsf config solution](https://gitlab.com/RiieCco1/training/blob/master/mobsf/mobsf-solution.md)

## Stage 5: Key Management

Now, we see that the config script has API keys all over the place checked in into your 
source control. Luckely for us most CI/CD environments have places where we can store these values.

Can you find where to do it and re-write your pipeline? 

Of course we can take this a step further and use a proper key management tool like hashicorp vault.
But sadly that is to much to cover for this course today.

If you like to play with vault, it can be found on:

```
http://35.229.114.152:8200/ui
```
And you can login with the root password:
```
s.gcXe0bZGIlqlfQoCn6Li3GQT
```
